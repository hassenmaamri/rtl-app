# RTL App

A simple SPA that loads TV Shows related to "Girls" from the TV Maze API. When a show is clicked, some information about the show is displayed, including its list of episodes. When an episode is clicked, some information about that episode is displayed.

---

## Technologies used

1. React (with create-react-app) and JavaScript (ES6).
2. Yarn for package managing.
3. FontAwesome, react-spring, Tailwindcss & PostCSS for CSS and UI.

---

## Installation and Running

1. Clone this project
2. CD into the directory `cd rtl-app`
3. Install all packages with `yarn` or `npm install`
4. Start the server with `yarn start` (this will also build the CSS) ... the app will then run on port 3000
