import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders rtl app link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/rtl app/i);
  expect(linkElement).toBeInTheDocument();
});
``