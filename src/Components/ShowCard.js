import React from 'react'
import {Link} from 'react-router-dom'

function ShowCard(props){
    return (
        <div className="border mb-4 rounded overflow-hidden">
            <Link to={`/shows/${props.show.id}`}>
                <div 
                    style={{
                        'backgroundImage': `url('${props.show.image.medium}')`,
                    }}
                    className="w-full h-64 bg-blue bg-cover"
                >
                </div>
            </Link>
            <div className="p-3">
                <h3 className="font-bold text-xl mb-3">
                    <Link to={`/shows/${props.show.id}`}>
                        { props.show.name }
                    </Link>    
                </h3>
                <div className="font-bold mb-3">
                    { props.show.type }
                </div>
                <div className="mb-3">
                    { props.show.summary == null ? "No summary is available for this show." : props.show.summary.replace(/<\/?[^>]+(>|$)/g, "").substring(0, 200)}
                </div>
                <Link 
                    to={`/shows/${props.show.id}`}
                    className="bg-blue-500 text-white p-2 flex justify-center w-full"
                >
                    View
                </Link>
            </div>
        </div>
    )
}

export default ShowCard