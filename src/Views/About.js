import React from 'react'

function About(){
    return (
        <div>
            <h1 className="font-bold text-2xl mb-3">About us</h1>
            <p>
            RTL Nederland is a subsidiary of the RTL Group. The media company is located in Hilversum. Although the licences of its TV-stations RTL 4, RTL 5, RTL 7, RTL 8, RTL Z, RTL Lounge, RTL Crime and RTL Telekids are issued by Luxembourg, the company targets the Dutch market.
            </p>
        </div>
    )
}

export default About