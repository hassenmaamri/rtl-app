import React from 'react'
import {Link} from 'react-router-dom'
import { useParams } from 'react-router-dom'
import Loader from '../Components/Loader'
import { useAxiosGet } from '../Hooks/HttpRequests'

function Episode(){
    const { id } = useParams()

    // Create your own Mock API: https://mockapi.io/
    const url = `http://api.tvmaze.com/episodes/${id}`

    
    let episode = useAxiosGet(url)

    let content = null

    if(episode.error){
        content = <div>
            <div className="bg-red-300 p-3">
                There was an error please refresh or try again later.
            </div>
        </div>
    }

    if(episode.loading){
        content = <Loader></Loader>
    }

    if(episode.data){
        content = 
        
        <div>
            <h1 className="text-2xl font-bold mb-3">
                {episode.data.name}
            </h1>
            <div>
                <img
                    src={episode.data.image.medium}
                    alt={episode.data.name}
                />
            </div>
            <div className="font-bold text-xl mb-3">
                Season: {episode.data.season}
            </div>
            <div>
                {episode.data.summary == null ? "No summary is available for this episode." : episode.data.summary.replace(/<\/?[^>]+(>|$)/g, "")}
            </div>
            <br></br>
        </div>
    }

    return (
        <div className="container mx-auto">
            {content}
        </div>
    )
}

export default Episode