import React from 'react'
import Loader from '../Components/Loader'
import ShowCard from '../Components/ShowCard'
import { useAxiosGet } from '../Hooks/HttpRequests'

function Home(){
    // Create your own Mock API: https://mockapi.io/
    const url = `http://api.tvmaze.com/search/shows?q=girls`
    let shows = useAxiosGet(url)

    let content = null

    if(shows.error){
        content = <div>
            <div className="bg-red-300 p-3">
                There was an error please refresh or try again later.
            </div>
        </div>
    }

    if(shows.loading){
        content = <Loader></Loader>
    }

    if(shows.data){
        content = 
        shows.data.map((show) => 
            <div key={show.show.id} className="flex-no-shrink w-full md:w-1/4 md:px-3">
                <ShowCard 
                    show={show.show}
                />
            </div>
        )
    }

    return (
        <div className="container mx-auto">
            <h1 className="font-bold text-2xl mb-3">
                Best Shows
            </h1>
            <div className="md:flex flex-wrap md:-mx-3">
                { content } 
            </div>
        </div>
    )
}

export default Home