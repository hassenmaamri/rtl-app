import React from 'react'
import {Link} from 'react-router-dom'
import { useParams } from 'react-router-dom'
import Loader from '../Components/Loader'
import { useAxiosGet } from '../Hooks/HttpRequests'

function Show(){
    const { id } = useParams()

    // Create your own Mock API: https://mockapi.io/
    const url = `http://api.tvmaze.com/shows/${id}`
    const epUrl = `http://api.tvmaze.com/shows/${id}/episodes`

    
    let show = useAxiosGet(url)
    let episodes = useAxiosGet(epUrl)


    let content = null
    let episodeList = null

    if(show.error || episodes.error){
        content = <div>
            <div className="bg-red-300 p-3">
                There was an error please refresh or try again later.
            </div>
        </div>
    }

    if(show.loading){
        content = <Loader></Loader>
    }

    if (show.data && episodes.data){
        if(Object.keys(episodes.data).length == 0){
            episodeList = "No episodes"
        } else{

            episodeList = 
            episodes.data.map((episode) => 
            <div key={episode.id}>
                <Link to={`/shows/${show.data.id}/episode/${episode.id}`} className="text-blue-500 py-3 border-b block">
                    {episode.name}
                </Link>
            </div>
            )
            
        }
    }
    if(show.data && episodes.data){
        content = 
        
        <div>
            <h1 className="text-2xl font-bold mb-3">
                {show.data.name}
            </h1>
            <div>
                <img
                    src={show.data.image.medium}
                    alt={show.data.name}
                />
            </div>
            <div className="font-bold text-xl mb-3">
                {show.data.type}
            </div>
            <div>
                {show.data.summary == null ? "No summary is available for this show." : show.data.summary.replace(/<\/?[^>]+(>|$)/g, "")}
            </div>
            <br></br>
            <h1 className="font-bold text-xxl mb-3">Episodes: </h1>
            {episodeList}
        </div>
    }

    return (
        <div className="container mx-auto">
            {content}
        </div>
    )
}

export default Show